/*
 * This file is part of the muphyII multiphysics plasma simulation project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "info_writer.h"
#include <fstream>
#include <iostream>
#include <stdexcept>

void InfoWriter::write_version(const std::string& output_file) {
    std::ofstream ofs(output_file);
    if (!ofs) {
        throw std::runtime_error("Failed to open output file: " + output_file);
    }
    
    ofs << "Build Mode: " << BUILD_MODE << "\n";
    ofs << "Build Timestamp: " << BUILD_TIMESTAMP << "\n";
    ofs << "Compiler Version: " << COMPILER_VERSION << "\n";
    ofs << "Code Version: " << VERSION << "\n";
    ofs << "Git Branch: " << GIT_BRANCH << "\n";
    ofs << "Last Commit Hash: " << LAST_COMMIT_HASH << "\n";
    ofs << "Last Commit Date: " << LAST_COMMIT_DATE << "\n";
    ofs << "Last Commit Author: " << LAST_COMMIT_AUTHOR << "\n";
    ofs << "Operating System: " << OS << "\n";
    ofs << "Architecture: " << ARCHITECTURE << "\n";
    ofs << "Hostname: " << HOSTNAME << "\n";
    ofs << "Username: " << USERNAME << "\n";
    ofs << "Software Stack:\n" << SOFTWARE_STACK << "\n";
    
    if (std::string(BUILD_MODE) == "develop") {
        ofs << "Git diff is available in a separate file.\n";
    } else {
        ofs << "Full source code archive is embedded.\n";
    }

    if (!ofs) {
        throw std::runtime_error("Failed to write to output file: " + output_file);
    }
}

void InfoWriter::write_git_patch(const std::string& output_file) {
//    if (std::string(BUILD_MODE) != "develop") {
//        throw std::runtime_error("Git patch is only available in develop mode");
//    }
#ifdef DEBUG
    std::ofstream ofs(output_file);
    if (!ofs) {
        throw std::runtime_error("Failed to open output file: " + output_file);
    }

    ofs << GIT_DIFF;

    if (!ofs) {
        throw std::runtime_error("Failed to write to output file: " + output_file);
    }
#else
    throw std::runtime_error("Git patch is only available in develop mode");
#endif
}

void InfoWriter::dump_source(const std::string& output_file) {
#ifdef DEBUG //    if (std::string(BUILD_MODE) != "release") {
        throw std::runtime_error("Source dump is only available in release mode");
#else
    std::ofstream ofs(output_file, std::ios::binary);
    if (!ofs) {
        throw std::runtime_error("Failed to open output file: " + output_file);
    }

    ofs.write(reinterpret_cast<const char*>(source_code_tar_gz), source_code_tar_gz_len);

    if (!ofs) {
        throw std::runtime_error("Failed to write to output file: " + output_file);
    }
#endif
}

void InfoWriter::copy_config(const std::string& source_file, const std::string& destination_file) {
    std::ifstream src(source_file, std::ios::binary);
    if (!src) {
        throw std::runtime_error("Failed to open source config file: " + source_file);
    }

    std::ofstream dst(destination_file, std::ios::binary);
    if (!dst) {
        throw std::runtime_error("Failed to open destination config file: " + destination_file);
    }

    dst << src.rdbuf();

    if (!dst) {
        throw std::runtime_error("Failed to write to destination config file: " + destination_file);
    }
}

