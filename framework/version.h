/*
 * This file is part of the muphyII multiphysics plasma simulation project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

extern "C" {
  extern const char* BUILD_MODE;
  extern const char* VERSION;
  extern const char* BUILD_TIMESTAMP;
  extern const char* COMPILER_VERSION;
  extern const char* OS;
  extern const char* ARCHITECTURE;
  extern const char* GIT_BRANCH;
  extern const char* LAST_COMMIT_HASH;
  extern const char* LAST_COMMIT_DATE;
  extern const char* LAST_COMMIT_AUTHOR;
  extern const char* SOFTWARE_STACK;
  extern const char* HOSTNAME;
  extern const char* USERNAME;
#ifdef DEBUG
  extern const char* GIT_DIFF;
#else
  extern const unsigned char source_code_tar_gz[];
  extern const unsigned int source_code_tar_gz_len;
#endif
}
