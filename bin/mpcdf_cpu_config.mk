# mpcdf_cpu_config.mk

# Required modules:
# module load gcc/10 openmpi/4.1

# Compiler and flags
MPICXX = mpicxx
MPIF90 = mpifort

# Flags
COMMON_FLAGS = -Ofast $(SETTINGS) -I$(SRCDIR)
ACC_FLAGS = 
CXX_FLAGS = $(COMMON_FLAGS) --std=c++11 -lgfortran -MMD -Wno-unknown-pragmas $(ACC_FLAGS)
FORTRAN_FLAGS = $(COMMON_FLAGS)  -ffree-line-length-none $(ACC_FLAGS)
LD_FLAGS = 


