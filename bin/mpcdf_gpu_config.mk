# mpcdf_gpu_config.mk

# Required modules:
# module load nvhpcsdk/24 cuda/12.3-nvhpcsdk_24 openmpi_gpu/4.1

# Compiler and flags
MPICXX = mpicxx
MPIF90 = mpifort

# Flags
COMMON_FLAGS = -fast $(SETTINGS) -I$(SRCDIR)
ACC_FLAGS = -acc -gpu,fastmath,keepgpu -cuda
CXX_FLAGS = $(COMMON_FLAGS) -std=c++17 -pgf90libs -MMD $(ACC_FLAGS)
FORTRAN_FLAGS = $(COMMON_FLAGS) $(ACC_FLAGS)
LD_FLAGS = 


