# build_config.mk

SETTINGS = 
SETTINGS += -DSINGLE_PRECISION #(single precision if set, double precision if not)
SETTINGS += -DACC_2D_OPT       #(optimize openacc pragmas for 2D instead of 3D)

SRCDIR = ..

# Compiler, flags, and library locations
include mpcdf_gpu_config.mk

# Executable name
EXEC = muphy2

# Overwrite Build mode (develop/release)
# BUILD_MODE = develop
