/*
 * This file is part of the muphyII multiphysics plasma simulation project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once
#include <string>
#include "version.h"

class InfoWriter {
public:
    static void write_version(const std::string& output_file);
    static void write_git_patch(const std::string& output_file);
    static void dump_source(const std::string& output_file);
    static void copy_config(const std::string& source_file, const std::string& destination_file);
};

